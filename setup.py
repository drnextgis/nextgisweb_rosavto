from setuptools import setup, find_packages
import sys, os

version = '0.0'

requires = (
    'nextgisweb',
    'geojson',
)

entry_points = {
    'nextgisweb.component': [
        'rosavto_feature_query = nextgisweb_rosavto.rosavto_feature_query',
        'rosavto_routing = nextgisweb_rosavto.rosavto_routing',
    ],
    'nextgisweb.amd_packages': [
        'nextgisweb_rosavto = nextgisweb_rosavto:amd_packages',
    ]
}

setup(
    name='nextgisweb_rosavto',
    version=version,
    description="",
    long_description="",
    classifiers=[],
    keywords='',
    author='',
    author_email='',
    url='',
    license='',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    entry_points=entry_points,
)
