define([
    "dojo/_base/declare",
    "ngw/modelWidget/Widget",
    "ngw/modelWidget/ErrorDisplayMixin",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./templates/Widget.html",
    // util
    "dojo/_base/array",
    // template
    "dijit/form/ValidationTextBox",
    "dojox/layout/TableContainer"
], function (
    declare,
    Widget,
    ErrorDisplayMixin,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,
    template,
    array
) {
    return declare([Widget, ErrorDisplayMixin, _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: template,
        identity: "ws_layer",
        title: "Слой WebSocket",

        postCreate: function () {
            this.inherited(arguments);

            if (this.value) {
                this.wSocketUrl.set("value", this.value.socket_url);
                this.wSubscribeUrl.set("value", this.value.subscribe_url);
                this.wFieldId.set("value", this.value.field_id);
                this.wFieldStyle.set("value", this.value.field_style);
            };
        },

        _getValueAttr: function () {
            var result = { 
                socket_url: this.wSocketUrl.get("value"),
                subscribe_url: this.wSubscribeUrl.get("value"),
                field_id: this.wFieldId.get("value"),
                field_style: this.wFieldStyle.get("value")
            };

            return result;
        },

        _setValueAttr: function (value) {
            this.wSocketUrl.set("value", value["socket_url"]);
            this.wSubscribeUrl.set("value", value["subscribe_url"]);
            this.wFieldId.set("value", value["field_id"]);
            this.wFieldStyle.set("value", value["field_style"]);
        },        

        validateWidget: function () {
            var widget = this;
            var result = { isValid: true, error: [] };

            array.forEach([this.wSocketUrl, this.wSubscribeUrl, this.wFieldId, this.wFieldStyle], function (subw) {
                // форсируем показ значка при проверке
                subw._hasBeenBlurred = true;
                subw.validate();   

                // если есть ошибки, фиксируем их
                if ( !subw.isValid() ) {
                    result.isValid = false;
                };
            });

            return result;
        }

    });
})