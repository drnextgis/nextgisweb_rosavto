# -*- coding: utf-8 -*-
import urllib
import urllib2

import geojson as json

from pyramid.response import Response
from pyramid.httpexceptions import HTTPNotFound, HTTPBadRequest

from osgeo import ogr, osr

from nextgisweb.views import model_context
from nextgisweb.geometry import geom_from_wkt, geom_from_wkb, box
from nextgisweb.feature_layer import Feature, FeatureSet
from nextgisweb.postgis_layer import PostgisLayer
from nextgisweb.layer import Layer

from UserList import UserList

class GeoJsonFeatureList(UserList):
    @property
    def __geo_interface__(self):
        return dict(
            type="FeatureCollection",
            features=[f.__geo_interface__ for f in self]
        )

def setup_pyramid(comp, config):

    class PostgisLayerWrapper(PostgisLayer):

        def __init__(self, layer):
            self.layer = layer

        @property
        def sp_query(self):
            class BoundFeatureQuery(StoredProcedureQueryBase):
                layer = self.layer
            return BoundFeatureQuery

    class StoredProcedureQueryBase(object):
        def __init__(self):
            self._getlrpos = None
            self._getlrposbyrid = None
            self._getlrposbyuuid = None
            self._getlrsublinebyuuid = None

        def getlrposbyuuid(self, **kwargs):
            self._getlrposbyuuid = kwargs

        def getlrsublinebyuuid(self, **kwargs):
            self._getlrsublinebyuuid = kwargs

        def __call__(self):
            columns = dict()

            if self._getlrposbyuuid:
                columns['geom'] = """ST_AsText(ST_Transform(ng_getlrposbyuuid('"%(schema)s"."%(table)s"', '%(guid)s', %(distance)f), %(srs)d))""" % dict(
                    schema=self.layer.schema,
                    table=self.layer.table,
                    srs=self.layer.srs_id,
                    **self._getlrposbyuuid
                )

            if self._getlrsublinebyuuid:
                columns['geom'] = """ST_AsText(ST_Transform(ng_getlrsublinebyuuid('"%(schema)s"."%(table)s"', '%(guid)s', %(first)f, %(last)f, %(step)f), %(srs)d))""" % dict(
                    schema=self.layer.schema,
                    table=self.layer.table,
                    srs=self.layer.srs_id,
                    **self._getlrsublinebyuuid
                )

            class QueryFeatureSet(FeatureSet):
                layer = self.layer
                _getlrposbyuuid = self._getlrposbyuuid
                _getlrsublinebyuuid = self._getlrsublinebyuuid

                def __iter__(self):
                    cols = []
                    for k, v in columns.iteritems():
                        cols.append('%s AS "%s"' % (v, k))                  

                    sql = "SELECT %(cols)s" % dict(cols=", ".join(cols))

                    conn = comp.env.postgis_layer.connection[self.layer.connection].connect()
                    result = conn.execute(sql)

                    for row in result:
                        yield Feature(
                            fields=dict(),
                            layer=self.layer,
                            geom=geom_from_wkt(row['geom']) if (self._getlrposbyuuid or self._getlrsublinebyuuid) else None
                        )

                    conn.close()

            return QueryFeatureSet()

    @model_context(Layer)
    def store_api(request, layer):
        request.require_permission(layer, 'data-read')

        # Преобразование данных из одной системы координат в другую
        def geometry_transform(geometry, source_srs, target_srs):
            source_osr = osr.SpatialReference()
            target_osr = osr.SpatialReference()

            source_osr.ImportFromEPSG(source_srs)
            target_osr.ImportFromEPSG(target_srs)

            if not source_osr.IsSame(target_osr):
                transform = osr.CoordinateTransformation(source_osr, target_osr)
                geom = ogr.CreateGeometryFromWkb(geometry.to_wkb())
                geom.Transform(transform)
                geometry = geom_from_wkb(geom.ExportToWkb())

            return geometry

        if 'guid' in request.GET:
            guid = str(request.GET['guid']).translate(None, "{}")
          
            # Сервис, возвращающий GeoJSON-представление точки, отстоящей на заданное
            # расстояние от начала сегмента, определяемого по GUID
            if 'distance' in request.GET:
                distance = float(request.GET['distance'])
                if layer.__class__.__name__ == 'PostgisLayer':
                    wrapper = PostgisLayerWrapper(layer)
                    query = wrapper.sp_query()
                    query.getlrposbyuuid(guid=guid, distance=distance)
                    try:
                        feature = query().one()
                    except:
                        raise HTTPNotFound()
                else:
                    raise HTTPNotFound()

            # Сервис, возвращающий GeoJSON-представление объекта слоя по переданному GUID
            elif 'distance' not in request.GET:
                
                if 'first' in request.GET and 'last' in request.GET:
                    first = float(request.GET['first'])
                    last = float(request.GET['last'])
                    step = float(request.GET.get('step', 1000))
                    if layer.__class__.__name__ == 'PostgisLayer':
                        wrapper = PostgisLayerWrapper(layer)
                        query = wrapper.sp_query()
                        query.getlrsublinebyuuid(guid=guid, first=first, last=last, step=step)
                        try:
                            feature = query().one()
                        except:
                            raise HTTPNotFound()
                    else:
                        raise HTTPNotFound()

                else:
                    query = layer.feature_query()
                    query.geom()
                    query.like(guid, 'uniq_uid')

                    try:
                        feature = query().one()
                    except:
                        raise HTTPNotFound()

            feature._geom = geometry_transform(feature.geom, int(layer.srs_id),
                    int(request.GET.get('srs', layer.srs_id)))

        else:
            query = layer.feature_query()
            query.geom()
            
            # Ограничиваем число запрашиваемых записей
            if 'limit' in request.GET:
                limit = int(request.GET['limit'])
                query.limit(limit=limit)

            # Фильтруем по охвату
            if 'bbox' in request.GET:
                bbox = map(float, request.GET['bbox'].split(','))
                geometry = box(*bbox, srid=4326)
                query.intersects(geometry)
            
            feature = GeoJsonFeatureList(query())
            for idx, f in enumerate(feature):
                feature[idx]._geom = geometry_transform(feature[idx].geom, int(layer.srs_id),
                    int(request.GET.get('srs', layer.srs_id)))
       
        return Response(
            json.dumps(feature),
            content_type='application/json'
        )

    config.add_route('rosavto_feature_query.store_api', '/layer/{id:\d+}/store_api/rosavto/')
    config.add_view(store_api, route_name='rosavto_feature_query.store_api')

    # 3. Функция определения географической точки по переданной
    # линейной координате и идентификатору дороги (GUID)
    # http://192.168.255.101:6543/layer/17/rosavto/getlrposbyuuid?guid={09B4B02F-A480-493a-9BFF-498D0CF39A04}&distance=12200
    @model_context(Layer)
    def ng_getlrposbyuuid(request, layer):
        request.require_permission(layer, 'data-read')

        if (('guid' in request.GET) and
            ('distance' in request.GET)):
            guid = str(request.GET['guid']).translate(None, "{}")
            distance = float(request.GET['distance'])

            sql = """SELECT ST_AsText(ng_getlrposbyuuid('"%(schema)s"."%(table)s"',
                    '%(guid)s', %(distance)f))""" % dict(
                schema=layer.schema,
                table=layer.table,
                guid=guid,
                distance=distance
            )

            try:
                conn = comp.env.postgis_layer.connection[layer.connection].connect()
                result = conn.execute(sql)

                return Response(
                    json.dumps(Feature(geom=geom_from_wkt(result.scalar()), fields=dict())),
                    content_type='application/json'
                )
            except:
                raise HTTPBadRequest()

        raise HTTPBadRequest()

    config.add_route('rosavto_feature_query.ng_getlrposbyuuid', '/layer/{id:\d+}/rosavto/getlrposbyuuid')
    config.add_view(ng_getlrposbyuuid, route_name='rosavto_feature_query.ng_getlrposbyuuid')


    # 4. Функция получения участка дороги по переданным линейным
    # координатам и идентификатору дороги (GUID)
    # http://192.168.255.101:6543/layer/17/rosavto/getlrsublinebyuuid?guid={09B4B02F-A480-493a-9BFF-498D0CF39A04}&first=1035&last=1099&step=1000
    @model_context(Layer)
    def ng_getlrsublinebyuuid(request, layer):
        request.require_permission(layer, 'data-read')

        if (('guid' in request.GET) and
            ('first' in request.GET) and
            ('last' in request.GET)):
            guid = str(request.GET['guid']).translate(None, "{}")
            first = float(request.GET['first'])
            last = float(request.GET['last'])
            step = float(request.GET.get('step', 1000))

            first, last = sorted((first, last))

            sql = """SELECT ST_AsText(ng_getlrsublinebyuuid('"%(schema)s"."%(table)s"',
                    '%(guid)s', %(first)f, %(last)f, %(step)f))""" % dict(
                schema=layer.schema,
                table=layer.table,
                guid=guid,
                first=first,
                last=last,
                step=step
            )

            try:
                conn = comp.env.postgis_layer.connection[layer.connection].connect()
                result = conn.execute(sql)

                return Response(
                    json.dumps(Feature(geom=geom_from_wkt(result.scalar()), fields=dict())),
                    content_type='application/json'
                )
            except:
                raise HTTPBadRequest()

        raise HTTPBadRequest()

    config.add_route('rosavto_feature_query.ng_getlrsublinebyuuid', '/layer/{id:\d+}/rosavto/getlrsublinebyuuid')
    config.add_view(ng_getlrsublinebyuuid, route_name='rosavto_feature_query.ng_getlrsublinebyuuid')

    # 5. Функция выбора участка дорожной сети по 2-м идентификаторам дорог (GUID)
    # и 2-м километровым отметкам
    # http://192.168.255.101:6543/layer/17/rosavto/getroutebyposuuid?guid_from={09B4B02F-A480-493a-9BFF-498D0CF39A04}&distance_from=12100&guid_to={09B4B02F-A480-493a-9BFF-498D0CF39A04}&distance_to=12200
    @model_context(Layer)
    def ng_getroutebyposuuid(request, layer):
        request.require_permission(layer, 'data-read')

        if (('guid_from' in request.GET) and
            ('distance_from' in request.GET) and
            ('guid_to' in request.GET) and
            ('distance_to' in request.GET)):
            guid_from = str(request.GET['guid_from']).translate(None, "{}")
            guid_to = str(request.GET['guid_to']).translate(None, "{}")
            distance_from = float(request.GET['distance_from'])
            distance_to = float(request.GET['distance_to'])

            sql = """SELECT ST_AsText(ng_getlrposbyuuid('"%(schema)s"."%(table)s"','%(guid)s', %(distance)f))"""

            sql_from = sql % dict(schema=layer.schema, table=layer.table, guid=guid_from, distance=distance_from)
            sql_to = sql % dict(schema=layer.schema, table=layer.table, guid=guid_to, distance=distance_to)

            try:
                conn = comp.env.postgis_layer.connection[layer.connection].connect()
                
                result_from = conn.execute(sql_from)
                result_to = conn.execute(sql_to)

                geom_from = geom_from_wkt(result_from.scalar())
                geom_to = geom_from_wkt(result_to.scalar())

                # /simple_routing не работает
                route_url = ':'.join(request.host_url.split(':')[:-1]) + ":5000/routing"

                query = dict(
                    from_x=geom_from.x,
                    from_y=geom_from.y,
                    to_x=geom_to.x,
                    to_y=geom_to.y
                )
                
                data = urllib2.urlopen('%s?%s' % (route_url, urllib.urlencode(query)))
                return Response(data.read(), content_type="application/json")

            except:
                raise HTTPBadRequest()

        raise HTTPBadRequest()

    config.add_route('rosavto_feature_query.ng_getroutebyposuuid', '/layer/{id:\d+}/rosavto/getroutebyposuuid')
    config.add_view(ng_getroutebyposuuid, route_name='rosavto_feature_query.ng_getroutebyposuuid')



    # 6. Функция получения пикетной отметки (километража)
    # по идентификатору дороги (GUID) и паре координат
    # http://192.168.255.101:6543/layer/17/rosavto/getlrdistbyuuid?guid={65de3f89-c234-44c5-867d-fd8961eb8644}&lon=36.46&lat=56.62
    @model_context(Layer)
    def ng_getlrdistbyuuid(request, layer):
        request.require_permission(layer, 'data-read')

        if (('guid' in request.GET) and
            ('lat' in request.GET) and
            ('lon' in request.GET)):
            guid = str(request.GET['guid']).translate(None, "{}")
            lat = float(request.GET['lat'])
            lon = float(request.GET['lon'])

            sql = """SELECT ng_getlrdistbyuuid('"%(schema)s"."%(table)s"',
                    '%(guid)s', %(lon)f, %(lat)f)""" % dict(
                schema=layer.schema,
                table=layer.table,
                guid=guid,
                lon=lon,
                lat=lat
            )

            try:
                conn = comp.env.postgis_layer.connection[layer.connection].connect()
                result = conn.execute(sql)

                return dict(distance=result.scalar())
            except:
                raise HTTPBadRequest()

        raise HTTPBadRequest()

    config.add_route('rosavto_feature_query.ng_getlrdistbyuuid', '/layer/{id:\d+}/rosavto/getlrdistbyuuid')
    config.add_view(ng_getlrdistbyuuid, route_name='rosavto_feature_query.ng_getlrdistbyuuid', renderer='json')

    # 7. Создание новых геометрий
    @model_context(Layer)
    def create(request, layer):
        request.require_permission(layer, 'data-edit')

        geom = request.json_body['geometry']
        guid = request.json_body.get('guid', '')
        conn = comp.env.postgis_layer.connection[layer.connection].connect()

        sql = """INSERT INTO "%(schema)s"."%(table)s"(%(geom_column)s, uniq_uid) VALUES (ST_GeomFromText('%(geom)s', 4326), '%(guid)s')
                    RETURNING uniq_uid""" % dict(
            schema=layer.schema,
            table=layer.table,
            geom_column=layer.column_geom,
            geom=geom,
            guid=guid
        )

        try:
            result = conn.execute(sql)
            row = result.first()
            if row:
                return dict(success=True, id=row.uniq_uid)
        except:
            return dict(success=False)

    config.add_route('rosavto_feature_query.create', '/layer/{id:\d+}/store_api/rosavto', request_method='POST')
    config.add_view(create, route_name='rosavto_feature_query.create', renderer='json')


