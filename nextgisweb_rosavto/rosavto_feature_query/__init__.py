# -*- coding: utf-8 -*-
from nextgisweb.component import Component

@Component.registry.register
class RosavtoFeatureQueryComponent(Component):
    identity = 'rosavto_feature_query'

    def initialize(self):
        super(RosavtoFeatureQueryComponent, self).initialize()

    def setup_pyramid(self, config):
        super(RosavtoFeatureQueryComponent, self).setup_pyramid(config)

        from . import views
        views.setup_pyramid(self, config)