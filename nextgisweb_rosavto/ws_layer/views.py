# -*- coding: utf-8 -*-

from nextgisweb.object_widget import ObjectWidget
from .models import WSLayer

def setup_pyramid(comp, config):

    class WSLayerObjectWidget(ObjectWidget):

        def is_applicable(self):
            return self.operation in ('create', 'edit')

        def populate_obj(self):
            ObjectWidget.populate_obj(self)

            self.obj.socket_url = self.data['socket_url']
            self.obj.subscribe_url = self.data['subscribe_url']
            self.obj.field_id = self.data['field_id']
            self.obj.field_style = self.data['field_style']

        def widget_params(self):
            result = ObjectWidget.widget_params(self)

            if self.obj:
                result['value'] = dict(
                    socket_url=self.obj.socket_url,
                    subscribe_url=self.obj.subscribe_url,
                    field_id=self.obj.field_id,
                    field_style=self.obj.field_style
                )

            return result

        def validate(self):
            result = ObjectWidget.validate(self)
            self.error = []
            return result

        def widget_module(self):
            return 'ws_layer/Widget'

    WSLayer.object_widget = WSLayerObjectWidget
