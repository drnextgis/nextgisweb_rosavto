from pkg_resources import resource_filename
from nextgisweb.component import Component, require
from nextgisweb.models import Base


@Component.registry.register
class WSLayerComponent(Component):
    identity = 'ws_layer'
    metadata = Base.metadata

    @require('layer')
    def initialize(self):
        super(WSLayerComponent, self).initialize()

    def setup_pyramid(self, config):
        super(WSLayerComponent, self).setup_pyramid(config)

        from . import views
        views.setup_pyramid(self, config)
