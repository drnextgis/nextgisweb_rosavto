# -*- coding: utf-8 -*-
import sqlalchemy as sa
from sqlalchemy import sql, func

from nextgisweb.models import declarative_base
from nextgisweb.layer import Layer

Base = declarative_base()

@Layer.registry.register
class WSLayer(Base, Layer):

    __tablename__ = 'ws_layer'

    identity = __tablename__
    cls_display_name = u"Cлой WebSocket"

    layer_id = sa.Column(sa.Integer, sa.ForeignKey(Layer.id), primary_key=True)
    socket_url = sa.Column(sa.Unicode, nullable=False)
    subscribe_url = sa.Column(sa.Unicode, nullable=False)
    field_id = sa.Column(sa.Unicode, nullable=False)
    field_style = sa.Column(sa.Unicode, nullable=False)

    __mapper_args__ = dict(
        polymorphic_identity=identity,
    )

    def get_info(self):
        return super(WSLayer, self).get_info() + (
            (u"URL", self.socket_url),
            (u"Строка подписки", self.subscribe_url),
            (u"Поле ID", self.field_id),
            (u"Поле стиля", self.field_style),
        )

