# -*- coding: utf-8 -*-

from nextgisweb.object_widget import ObjectWidget
from .models import WSStyle


def setup_pyramid(comp, config):

    class WSStyleObjectWidget(ObjectWidget):

        def widget_module(self):
            return 'ws_style/Widget'

    WSStyle.object_widget = WSStyleObjectWidget