# -*- coding: utf-8 -*-
import sqlalchemy as sa

from nextgisweb.models import declarative_base
from nextgisweb.layer import Layer
from nextgisweb.style import Style

Base = declarative_base()

@Style.registry.register
class WSStyle(Base, Style):

    __tablename__ = 'ws_style'

    identity = __tablename__
    cls_display_name = u"WS стиль"

    style_id = sa.Column(sa.ForeignKey(Style.id), primary_key=True)

    __mapper_args__ = dict(
        polymorphic_identity=identity,
    )

    widget_module = 'ws_style/Widget'

    @classmethod
    def is_layer_supported(cls, layer):
        return layer.cls == 'ws_layer'
