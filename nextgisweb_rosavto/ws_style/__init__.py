# -*- coding: utf-8 -*-
from nextgisweb.component import Component

from .models import Base, WSStyle

__all__ = ['WSStyleComponent', 'WSStyle']

@Component.registry.register
class WSStyleComponent(Component):
    identity = 'ws_style'
    metadata = Base.metadata

    def setup_pyramid(self, config):
        from . import views
        views.setup_pyramid(self, config)